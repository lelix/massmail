.. -*- coding: utf-8 -*-
.. :Project:   MassMail
.. :Created:   lun 16 nov 2020, 17:46:00
.. :Author:    Lele Gaifax <lele@metapensiero.it>
.. :License:   GNU General Public License version 3 or later
.. :Copyright: © 2020 Lele Gaifax
..

==========
 MassMail
==========

This is a very simple program I wrote ten years ago, as a quick solution to send Merry
Christmas greetings to a set of email addresses.

I slightly modernized the code porting it to Python 3.

Usage
-----

Put the destination email addresses into a text file, one address per line, say
``addresses.txt``::

  lele@example.com
  lele@example.it

Write the message text into another file, say ``message.txt``::

  Hi, this is a test!

Then you can for example say::

  massmail.py -n -s Foo -m message.txt -N me -A me@example.com addresses.txt

The options may be stored in a file too; the above could be obtained by creating a text file,
say ``options.txt``::

  --subject=Foo
  -m=message.txt
  -N=me
  -A=me@example.com
  addresses.txt

and then executing::

  massmail.py --dry-run @options.txt
