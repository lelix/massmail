# -*- coding: utf-8 -*-
# :Progetto:  MassMail
# :Creato:    ven 03 dic 2010 16:23:20 CET
# :Autore:    Lele Gaifax <lele@metapensiero.it>
# :Licenza:   GNU General Public License version 3 or later
#

def prepare_message(args):
    from email.message import EmailMessage
    from email.headerregistry import Address
    from email.utils import formatdate

    msg = EmailMessage()
    msg['Date'] = formatdate()
    msg['Subject'] = args.subject
    msg['From'] = Address(args.from_name, addr_spec=args.from_address)

    msg.set_content(args.text_message.read_text())

    if args.html_message:
        msg.preamble = 'This is a multi-part message in MIME format.'
        msg.add_alternative(args.html_message.read_text(), subtype='html')

    return msg


def send_messages(args):
    import smtplib
    import socket
    from time import sleep

    message = prepare_message(args)
    with args.addresses.open() as f:
        addresses = sorted(set(line.strip() for line in f.readlines()))

    batch_size = int(args.batch_size)
    try:
        start_from = int(args.start_from)
    except ValueError:
        start_from = addresses.index(args.start_from)

    interbatch_wait = args.wait and int(args.wait)

    print("Collected %d different destinations" % len(addresses))

    if not args.dry_run:
        if args.smtp_ssl:
            smtp = smtplib.SMTP_SSL(args.smtp_server, port=args.smtp_port)
        else:
            smtp = smtplib.SMTP(args.smtp_server, port=args.smtp_port)
        if args.smtp_starttls:
            smtp.starttls()
        if args.smtp_user:
            smtp.login(args.smtp_user, args.smtp_password)

    if args.dry_run:
        if not args.addresses_only:
            print(message.as_string())

    i = 0
    for index in range(start_from, len(addresses), batch_size):
        batch = addresses[index:index+batch_size]
        print("Sending to %d addresses starting from %d, from <%s> to <%s> ..."
              % (len(batch), index, batch[0], batch[-1]))
        retry = 3
        if not args.dry_run:
            while retry > 0:
                try:
                    smtp.sendmail(args.from_address, batch, message.as_string())
                except (socket.error, smtplib.SMTPRecipientsRefused) as e:
                    print("ERROR: ", e)

                    if 1 < retry < 3:
                        "--> take a longer break..."
                        interbatch_wait *= 2
                    retry = retry - 1
                    if retry:
                        print("Sending error, retrying...")
                        if interbatch_wait:
                            print("... sleeping for %s seconds" % interbatch_wait)
                            sleep(interbatch_wait)
                    else:
                        print("Sending error, giving up!")
                except smtplib.SMTPServerDisconnected:
                    if args.smtp_ssl:
                        smtp = smtplib.SMTP_SSL(args.smtp_server)
                    else:
                        smtp = smtplib.SMTP(args.smtp_server)
                    if args.smtp_user:
                        smtp.login(args.smtp_user, args.smtp_password)
                else:
                    if retry > 0 and interbatch_wait and len(batch) == batch_size:
                        print("... sleeping for %s seconds" % interbatch_wait)
                        sleep(interbatch_wait)
                    break
            if retry > 0 and interbatch_wait and len(batch) == batch_size:
                interbatch_wait = args.wait and int(args.wait)

        elif args.addresses_only:
            print("Would send to ...")
            for i, addr in enumerate(batch, i+1):
                print(" %5d: %s " % (i, addr))
            if interbatch_wait and len(batch) == batch_size:
                print("--> Just assume I'll sleep for the next %s seconds" % interbatch_wait)

        if not retry:
            break

    if not args.dry_run:
        smtp.quit()


def main():
    from argparse import ArgumentParser
    from pathlib import Path

    parser = ArgumentParser(description="Send an email to may recipients",
                            fromfile_prefix_chars='@',
                            epilog="""\
You may store the options in a text file, one per line, and
then reference that file name preceeded by a “@”, for example
“massmail.py --dry-run ... @options.txt”.
""")
    parser.add_argument("-s", "--subject", help="the email subject")
    parser.add_argument("-m", "--text-message", type=Path, required=True,
                        help="the file containing the textual body of the message")
    parser.add_argument("-M", "--html-message", type=Path,
                        help="the file containing the HTML body of the message")
    parser.add_argument("-b", "--batch-size", default=50, type=int,
                        help="send messages in batches, by default 50")
    parser.add_argument("-f", "--start-from", default=0, type=int,
                        help="start from the Nth address (zero based),"
                        " by default from the first; may specify also"
                        " one particular email address instead of its"
                        " position")
    parser.add_argument("-w", "--wait", default=30, type=int,
                        help="wait for X seconds after each batch,"
                        " by default 30 seconds")
    parser.add_argument("-n", "--dry-run", action="store_true", default=False,
                        help="just print the raw message")
    parser.add_argument("-a", "--addresses-only", action="store_true", default=False,
                        help="just print the addresses, instead of the raw message")
    parser.add_argument("-N", "--from-name", required=True, help="the name of the sender")
    parser.add_argument("-A", "--from-address", required=True,
                        help="the email address of the sender")
    parser.add_argument("--smtp-server", default="localhost",
                        help="set the SMTP server, if needed")
    parser.add_argument("--smtp-port", help="set the SMTP port", default=0, type=int)
    parser.add_argument("--smtp-user", help="set the SMTP user, if needed")
    parser.add_argument("--smtp-password", help="set the SMTP password")
    parser.add_argument("--smtp-ssl", action="store_true", default=False,
                        help="connect thru SSL")
    parser.add_argument("--smtp-starttls", action="store_true", default=False,
                        help="start TLS")
    parser.add_argument('addresses', type=Path,
                        help="file containing destination addresses, one per line")

    args = parser.parse_args()

    if args.addresses_only:
        args.dry_run = True

    send_messages(args)


if __name__ == '__main__':
    main()
